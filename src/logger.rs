use crate::utils::{get_required_env_var, ok_or_exit};
use log::LevelFilter;
use simplelog::{CombinedLogger, ConfigBuilder, SimpleLogger, WriteLogger};
use std::fs::File;

pub fn set() {
    let log_file = get_required_env_var("LOG_FILE");
    let f = ok_or_exit(File::create(&log_file), Some(log_file));

    let config = ConfigBuilder::new()
        .set_time_format_str("[%Y-%m-%d %H:%M:%S%.f]")
        .build();
    let level_filter = LevelFilter::Info;

    ok_or_exit(
        CombinedLogger::init(vec![
            SimpleLogger::new(level_filter, config.clone()),
            WriteLogger::new(level_filter, config, f),
        ]),
        None,
    );
}
