// https://stackoverflow.com/questions/48430836/rust-proper-error-handling-auto-convert-from-one-error-type-to-another-with-que
use thiserror::Error;

#[derive(Error, Debug)]
pub enum Errors {
    #[error("Days threshold must be a positive number, not \"{0}\"")]
    PositiveThreshold(i64),
    #[error(transparent)]
    Reqwest(#[from] reqwest::Error),
    // Serde(#[from] serde::Error),
}
