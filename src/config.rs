use serde::Deserialize;
use validator::Validate;

#[derive(Debug, Validate, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct Discord {
    pub webhook: String,
    // TODO: Find a way to check field values with Serde directly
    // #[serde(default = "default_discord_field_limit")]
    #[validate(range(min = 1, max = 10, code = "code_str", message = "TBD"))]
    pub field_limit: u64,
}

#[derive(Debug, Validate, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct Sonarr {
    pub base_url: String,
    pub api_key: String,
    // #[serde(default = "default_sonarr_days_threshold")]
    #[serde(default = "default_sonarr_days_threshold")]
    pub days_threshold: i64,
    #[serde(default = "default_sonarr_page_size")]
    pub page_size: u64,
}

#[derive(Debug, Validate, Deserialize, Clone)]
#[serde(deny_unknown_fields)]
pub struct Config {
    #[validate]
    pub discord: Option<Discord>,
    #[validate]
    pub sonarr: Sonarr,
}

fn default_sonarr_days_threshold() -> i64 {
    7
}

fn default_sonarr_page_size() -> u64 {
    30
}

/// It seems Discord limit this to 24
fn default_discord_field_limit() -> usize {
    24
}
