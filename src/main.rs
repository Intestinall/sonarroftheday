use job_scheduler::{Job, JobScheduler};

#[macro_use]
extern crate log;

mod config;
pub mod connector;
pub mod error;
mod logger;
mod run;
mod sonarr;
pub mod sonarr_json;
pub mod utils;

fn main() {
    logger::set();

    let conf = utils::get_configuration_file();

    let mut sched = JobScheduler::new();
    let job = utils::get_schedule();
    let cron_delay = utils::get_cron_delay(&job);

    sched.add(Job::new(job, move || {
        match run::run(conf.clone()) {
            Ok(v) => info!("Successfully reported {} missing elements", v),
            Err(e) => error!("{}", e),
        };
    }));

    loop {
        sched.tick();
        std::thread::sleep(cron_delay);
    }
}
