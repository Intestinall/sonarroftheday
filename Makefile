.PHONY: format lint

format:
	cargo fmt

lint: format
	cargo clippy -- -W clippy::pedantic
