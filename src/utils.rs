use crate::config::Config;
use chrono::Local;
use job_scheduler::Schedule;
use std::fs;
use std::process::exit;
use std::time::Duration;
use validator::{Validate, ValidationError, ValidationErrors, ValidationErrorsKind};

#[must_use]
pub fn get_schedule() -> Schedule {
    let value = get_required_env_var("JOB_SCHEDULE");
    ok_or_exit(value.parse(), Some(value))
}

#[must_use]
pub fn get_configuration_file() -> Config {
    let value = get_required_env_var("CONFIGURATION_PATH");
    let file_content = ok_or_exit(fs::read_to_string(&value), Some(value.clone()));
    let conf: Config = ok_or_exit(serde_yaml::from_str(&file_content), Some(value));
    // if let Err(e) = conf.validate() {
    //     let e: ValidationErrors = e;
    //     let mut ma= e.into_errors();
    //     ma.retain(|key, value| {
    //         match value {
    //             ValidationErrorsKind::Field(v) => {
    //                 // println!("{} / {}", key, lol);
    //                 println!("{} / {}", key, v.len());
    //             },
    //             _ => println!("OK...")
    //         };
    //
    //         true
    //     });
    //
    //     // println!("{}", e.get("discord").unwrap());
    //     exit(1);
    // }
    ok_or_exit(conf.validate(), None);
    conf
}

#[must_use]
pub fn get_required_env_var(key: &str) -> String {
    ok_or_exit(std::env::var(&key), Some(key.to_string()))
}

#[must_use]
pub fn get_cron_delay(job: &Schedule) -> Duration {
    let mut iter = job.upcoming(Local);
    let time1 = iter.next().expect("This should never happen.");
    let time2 = iter.next().expect("This should never happen.");
    ok_or_exit((time2 - time1).to_std(), None) - Duration::from_secs(1)
}

pub fn ok_or_exit<T, E: ToString>(result: Result<T, E>, error_msg: Option<String>) -> T {
    match result {
        Ok(v) => v,
        Err(e) => {
            if let Some(msg) = error_msg {
                println!("[FATAL ERROR] {} : {}", e.to_string(), msg)
            } else {
                println!("[FATAL ERROR] {}", e.to_string())
            }
            exit(1)
        }
    }
}
