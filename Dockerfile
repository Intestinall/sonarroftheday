FROM ekidd/rust-musl-builder:latest AS builder

COPY Cargo.toml Cargo.lock ./
COPY src src
RUN cargo build --release

FROM alpine:latest
COPY --from=builder /home/rust/src/target/x86_64-unknown-linux-musl/release/sonarroftheday .

ENTRYPOINT ["./sonarroftheday"]
