use chrono::{DateTime, Utc};
use serde::Deserialize;

#[derive(Deserialize)]
pub struct SonarrWantedMissingRecordSeries {
    #[serde(rename = "title")]
    pub title: String,
    #[serde(rename = "tvdbId")]
    pub tvdb_id: u32,
}

#[derive(Deserialize)]
pub struct SonarrWantedMissingRecord {
    #[serde(rename = "seasonNumber")]
    pub season_number: u32,
    #[serde(rename = "episodeNumber")]
    pub episode_number: u32,
    pub title: String,
    #[serde(rename = "airDateUtc")]
    pub air_date_utc: DateTime<Utc>,
    pub series: SonarrWantedMissingRecordSeries,
    #[serde(rename = "lastSearchTime")]
    last_search_time: Option<String>, // 2020-11-22T18:01:24.767371Z
}

#[derive(Deserialize)]
pub struct SonarrWantedMissing {
    #[serde(rename = "pageSize")]
    pub page_size: u32,
    pub records: Vec<SonarrWantedMissingRecord>,
    #[serde(rename = "totalRecords")]
    pub total_records: u32,
}

impl SonarrWantedMissingRecord {
    #[must_use]
    pub fn pretty_title(&self) -> String {
        format!(
            "{} - {}x{} - {}",
            self.series.title, self.season_number, self.episode_number, self.title,
        )
    }

    #[must_use]
    pub fn tvdb_url(&self) -> String {
        format!("http://thetvdb.com/?tab=series&id={}", self.series.tvdb_id)
    }
}
