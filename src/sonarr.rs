use crate::config;
use crate::error::Errors;
use crate::error::Errors::PositiveThreshold;
use crate::sonarr_json::{SonarrWantedMissing, SonarrWantedMissingRecord};
use chrono::{DateTime, Duration, Local};
use reqwest::blocking::{Client, RequestBuilder};

pub struct Sonarr {
    client: Client,
    config: config::Sonarr,
}

impl Sonarr {
    pub fn new(mut config: config::Sonarr) -> Result<Sonarr, Errors> {
        // TODO: Find a way to check field values with Serde directly
        config.days_threshold = Sonarr::check_threshold(config.days_threshold)?;
        Ok(Sonarr {
            client: Client::new(),
            config,
        })
    }

    fn check_threshold(string_threshold: i64) -> Result<i64, Errors> {
        if string_threshold >= 0 {
            Ok(string_threshold)
        } else {
            Err(PositiveThreshold(string_threshold))
        }
    }

    fn get(&self, endpoint: &str) -> RequestBuilder {
        let url = format!("{}/api/{}", self.config.base_url, endpoint);
        self.client
            .get(&url)
            .query(&[("apikey", &self.config.api_key)])
    }

    pub fn get_page(&self, n: u32) -> Result<SonarrWantedMissing, Errors> {
        // TODO : Find a better way to handle
        //  [ERROR] error decoding response body: missing field `pageSize` at line 3 column 1
        //  when wrong api_key
        Ok(self
            .get("wanted/missing")
            .query(&[
                ("sortKey", "airDateUtc"),
                ("pageSize", &self.config.page_size.to_string()),
                ("page", &n.to_string()),
            ])
            .send()?
            .json()?)
    }

    pub fn get_threshold(&self) -> DateTime<Local> {
        Local::now() - Duration::days(self.config.days_threshold)
    }

    pub fn get_wanted_missing(&self) -> Result<Vec<SonarrWantedMissingRecord>, Errors> {
        let threshold = self.get_threshold();

        let mut records: Vec<SonarrWantedMissingRecord> = Vec::new();
        let mut n = 1;

        // TODO : there must be a cleaner way
        loop {
            let wanted_missing_page = self.get_page(n)?;

            for page_record in wanted_missing_page.records {
                if page_record.air_date_utc < threshold {
                    records.push(page_record);
                } else {
                    break;
                }
            }

            if wanted_missing_page.total_records <= n * wanted_missing_page.page_size {
                break;
            }
            n += 1;
        }

        Ok(records)
    }
}
