use crate::config::Config;
use crate::connector;
use crate::error::Errors;
use crate::sonarr::Sonarr;

pub fn run(conf: Config) -> Result<usize, Errors> {
    let sonarr = Sonarr::new(conf.sonarr)?;

    let wm = sonarr.get_wanted_missing()?;

    if let Some(discord_config) = conf.discord {
        let discord = connector::Discord::new(discord_config);
        discord.send(&wm)?
    }

    Ok(wm.len())
}
