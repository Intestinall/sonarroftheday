use crate::config;
use crate::error::Errors;
use crate::sonarr_json::SonarrWantedMissingRecord;
use reqwest::blocking::Client;
use serde_json::{json, Value};

pub struct Connector {
    client: Client,
    config: config::Discord,
}

impl Connector {
    #[must_use]
    pub fn new(config: config::Discord) -> Connector {
        Connector {
            client: Client::new(),
            config,
        }
    }

    fn record_to_json(r: &SonarrWantedMissingRecord) -> Value {
        // TODO : use a better Datetime string format
        let value = format!("Air date: {}\nTVDB link: {}", r.air_date_utc, r.tvdb_url());
        json!({
            "name": r.pretty_title(),
            "link": r.tvdb_url(),
            "value": value,
            "inline": false
        })
    }

    fn get_json(wmi: &[SonarrWantedMissingRecord], loop_count: usize, upper_bound: usize) -> Value {
        let records: Vec<Value> = wmi.iter().map(Connector::record_to_json).collect();

        json!({
            "embeds": [
              {
                "title": format!("The following content is still not downloaded ! ({}/{})", loop_count, upper_bound),
                "color": 1_127_128,
                "fields": records
              },
            ],
        })
    }

    pub fn send(&self, wmi: &[SonarrWantedMissingRecord]) -> Result<(), Errors> {
        let upper_bound = (wmi.len() / self.config.field_limit as usize) + 1;
        let mut loop_count: usize = 1;

        for records in wmi.chunks(self.config.field_limit as usize) {
            let json_body = Connector::get_json(records, loop_count, upper_bound);
            loop_count += 1;

            self.client
                .post(&self.config.webhook)
                .json(&json_body)
                .send()?;
        }
        Ok(())
    }
}
